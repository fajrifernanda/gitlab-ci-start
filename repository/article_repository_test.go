package repository

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/fannyhasbi/article-service/model"
)

type ArticleServiceSuccess struct{}

func (ps ArticleServiceSuccess) CheckSlugIsExist(slug string) bool {
	return false
}

func (ps ArticleServiceSuccess) GetPublishedArticleBySlug(slug string) (model.Article, error) {
	return model.Article{}, nil
}

func TestCanSaveArticleIntoStorage(t *testing.T) {
	// given
	articleService := &ArticleServiceSuccess{}
	article1, _ := model.CreateArticle(articleService, "Juragan Bakso", "juragan-bakso", "Baksooooo", model.News)
	article2, _ := model.CreateArticle(articleService, "FAQ", "faq", "Frequently Asked Questions", model.Politic)

	repo := CreateArticleStorage()

	// when
	repo.SaveArticle(&article1)
	repo.SaveArticle(&article2)

	// then
	assert.Equal(t, repo.ArticleMap, map[string]model.Article{
		article1.Slug: article1,
		article2.Slug: article2,
	})
}
