package model

import (
	"errors"
	"fmt"
	"strings"
	"time"
)

// Article store an article
type Article struct {
	Slug      string
	Name      string
	Body      string
	CreatedAt time.Time
	Category  []Category
	Status    ArticleStatus
}

type PostService interface {
	CheckSlugIsExist(slug string) bool
	GetPublishedArticleBySlug(slug string) (Article, error)
}

type Category string
type ArticleStatus string

const (
	Politic       Category = "politic"
	News          Category = "news"
	Internet      Category = "internet"
	Entertainment Category = "entertainment"
)

const (
	ArticlePublished    ArticleStatus = "published"
	ArticleNotPublished ArticleStatus = "notPublished"
)

// CreateArticle create an article
func CreateArticle(ps PostService, name, slug, body string, categories ...Category) (Article, error) {
	//WHEN
	if len(categories) == 0 {
		return Article{}, fmt.Errorf("Category needed")
	}

	slugExist := ps.CheckSlugIsExist(name)
	if len(slug) == 0 {
		temp := strings.Fields(name)
		slug = strings.Join(temp, "-")
		timeStamp := time.Now().UnixNano()
		timeString := fmt.Sprintf("%d", timeStamp)
		slug += "-" + timeString
	} else {
		if slugExist {
			return Article{}, errors.New("Change slug, slug is already used")
		}
	}

	for _, category := range categories {
		switch category {
		case Politic, News, Internet, Entertainment:
		default:
			return Article{}, fmt.Errorf("Category not valid")
		}
	}

	//THEN
	return Article{
		Name:      name,
		Slug:      slug,
		Body:      body,
		CreatedAt: time.Now(),
		Category:  categories,
		Status:    ArticleNotPublished,
	}, nil
}
